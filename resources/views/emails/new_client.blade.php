<!doctype html>
<html>
    <head>
        <style>
            th, td { padding: 5px; }
            th { text-align: right; }
            tr:nth-of-type(even) th, tr:nth-of-type(even) td { background: rgb(235,235,235); }
        </style>
    </head>
    <body>
        <table>
            @foreach ($data as $key => $value)
                <tr>
                    <th>{{ $key }}</th>
                    <td>{{ $value }}</td>
                </tr>
            @endforeach
        </table>
    </body>
</html>
