<!doctype html>
<html ng-app="app">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ config('site.title') }}</title>
        <link href="https://fonts.googleapis.com/css?family=Oswald:300,500" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="/css/theme.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
        <script src="/js/functions.js"></script>
        @section('header_scripts')
        @show
        @include('layouts.tracking')
    </head>
    <body>
        <header id="header">
            <div class="logo">
                <a href="/"><img src="/img/logo-white.png" alt="{{ config('site.company_name') }}"></a>
            </div>
            <nav id="header-nav">
                <li><a href="#about">About</a></li>
                <li><a href="#services">Services</a></li>
                <li><a href="#contact">Pricing</a></li>
                <li><a href="#contact">Contact</a></li>
            </nav>
            <div class="phone">
                <i class="fa fa-phone"></i> &nbsp;{{ config('site.phone') }}
            </div>
            <button class="toggle-nav" onclick="toggleNav('#nav')">
                <i class="fa fa-bars"></i>
            </button>
        </header>
        <div id="phone-small-screen">
            <i class="fa fa-phone"></i> &nbsp;{{ config('site.phone') }}
        </div>
        <nav id="nav">
            <li><a href="#about">About</a></li>
            <li><a href="#services">Services</a></li>
            <li><a href="#contact">Pricing</a></li>
            <li><a href="#contact">Contact</a></li>
        </nav>
        @section('content')
        @show
        <footer>
            <img src="/img/logo-green.png" alt="{{ config('site.company_name') }}" width="150">
            <br>
            <br>
            <nav>
                <li><a href="#about">About</a></li>
                <li><a href="#services">Services</a></li>
                <li><a href="#contact">Pricing</a></li>
                <li><a href="#contact">Contact</a></li>
            </nav>
            <div id="copyright">&copy; {{ date('Y') }} {{ config('site.company_name') }}</div>
        </footer>
        @section('footer_scripts')
        @show
    </body>
</html>
