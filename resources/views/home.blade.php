@extends('layouts.default')
@section('header_scripts')
    <script src="/js/controllers/HomeController.js"></script>
@stop
@section('content')
    <div ng-controller="HomeController">
        <section id="hero">
            <div class="content">
                <div>
                    @if ($local)
                        <h3>Utah's Leading Web Design and Development Service Company</h3>
                    @endif
                    <h1>We Build Next-generation Web Apps</h1>
                    <ul id="types">
                        <li>eCommerce</li>
                        <li>Web Sites</li>
                        <li>Business Intelligence</li>
                        <li>Corporate Software</li>
                        <li>Mobile Friendly</li>
                    </ul>
                    <br>
                    <br>
                    <a href="#contact" class="button white">Get a Free Quote</a>
                </div>
            </div>
        </section>
        <section id="ceo">
            <div>
                <p>
                    <img width="200" class="pull-left" src="/img/home/stephen-gashler.jpg">
                    <strong>Hi, I'm Steve, CEO and lead engineer of {{ config('site.company_name') }}.</strong> With a background in graphic design, marketing, and business development, I'm passionate about helping companies grow by outfitting them with the technology that makes it possible.
                    @if ($local)
                        Have a big project? Let me take you to lunch, and let's talk about. We'll
                    @else
                        Let's
                    @endif
                    discuss how {{ config('site.company_name') }} can help you achieve your goals. Give me a call at <strong>{{ config('site.phone') }}</strong> or use the contact form to get a free quote.</p>
                </p>
            </div>
            <div id="contact">
                <h2>
                    <span ng-show="!package">Get a Free Quote</span>
                    <span ng-show="package">Get Started</span>
                </h2>
                <div class="alert success" ng-if="package">
                    <strong>Your Package:</strong> @{{ package.name }} - $@{{ package.price }}
                    <ul>
                        <li class="fa fa-check" ng-repeat="service in services" ng-show="inArray($index, package.services)">@{{ service }}</li>
                    </ul>
                </div>
                <p>Fill out the form below or call <strong>{{ config('site.phone') }}</strong> to talk to a representative.</p>
                <p>(This form is for people who are interested in learning more about our services. Please don't contact us with business offers.)</p>
                <form ng-submit="submitForm()">
                    <input ng-model="form.Name" type="text" placeholder="Your Name">
                    <input ng-model="form.Company" type="text" placeholder="Your Company Name">
                    <input ng-model="form.Position" type="text" placeholder="Your Position">
                    <input ng-model="form.Email" type="text" placeholder="Your Email Address">
                    <input ng-model="form.Phone" type="text" placeholder="Your Phone Number (optional)">
                    <textarea ng-model="form.Comments" rows="10" placeholder="Tell us about your project and what you're looking for. The more information you can give, the better our estimate will be."></textarea>
                    <button ng-show="!submitting && !submitted" class="button">
                        <span ng-show="!package">Send Me My Free Quote</span>
                        <span ng-show="package">Get Started</span>
                    </button>
                    <img class="loading" ng-show="submitting" src="/img/loading.gif">
                </form>
            </div>
        </section>
        <section id="about" class="inverted">
            <div id="icons">
                <h2>We'll Make it Happen</h2>
                <p>Whether you have an existing system or are wanting to start from the ground up, we've got you covered. We're passionate about beautiful code, beautiful design, and beautiful results.
                <div class="icons">
                    <div>
                        <i class="fa fa-pencil"></i>
                        <div>Plan</div>
                    </div>
                    <i class="fa fa-long-arrow-right"></i>
                    <div>
                        <i class="fa fa-paint-brush"></i>
                        <div>Design</div>
                    </div>
                    <i class="fa fa-long-arrow-right"></i>
                    <div>
                        <i class="fa fa-code"></i>
                        <div>Build</div>
                    </div>
                    <i class="fa fa-long-arrow-right"></i>
                    <div>
                        <i class="fa fa-bug"></i>
                        <div>Test</div>
                    </div>
                    <i class="fa fa-long-arrow-right"></i>
                    <div>
                        <i class="fa fa-rocket"></i>
                        <div>Launch</div>
                    </div>
                </div>
            </div>
            <div id="description">
                <h2>Built the Way You Want</h2>
                <p class="margin-top-0">
                    <strong>{{ config('site.company_name') }} is a custom software development firm
                    @if($local)
                        based in Provo, Utah,
                    @endif
                    </strong> specializing in web applications. From simple web sites to robust web apps, if you can dream it, we can build it. We utilize the latest technologies to help companies sell more and do more.
                </p>
                <a href="#contact" class="button">Get a Free Quote</a>
            </div>
        </section>
        <section id="services">
            <div id="technologies" style="width: 17%">
                <h2>Technologies</h2>
                <ul>
                    <li>PHP</li>
                    <li>Laravel</li>
                    <li>HTML</li>
                    <li>CSS</li>
                    <li>JavaScript</li>
                    <li>AngularJS</li>
                    <li>VueJS</li>
                    <li>MySQL</li>
                    <li>NginX</li>
                </ul>
            </div>
            <div id="platforms" style="width: 17%">
                <h2>Platforms</h2>
                <ul>
                    <li>Wordpress</li>
                    <li>Joomla</li>
                    <li>Magento</li>
                    <li>Amazon Web Services</li>
                    <li>Digital Ocean</li>
                    <li>Sparkpost</li>
                    <li>Mailgun</li>
                    <li>Mailchimp</li>
                </ul>
            </div>
            <div id="services-list" style="width: 17%">
                <h2>Services</h2>
                <ul>
                    <li>Architecture Design</li>
                    <li>Graphic Design</li>
                    <li>Development</li>
                    <li>Deployment and Hosting</li>
                    <li>Updates and Long-term Support</li>
                </ul>
            </div>
            <div id="systems" style="width: 50%">
                <h2>Web Apps &amp; Solutions</h2>
                <p>Examples of the systems and modules we've built for clients:</p>
                <div
                    ng-click="toggleModule(module)"
                    ng-repeat="module in modules" class="@{{ module.class }}"
                >
                    <div>
                        <div>
                            <i class="fa fa-@{{ module.icon }}"></i>
                            <div>@{{ module.name }}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="popup" id="modulePopup" ng-click="togglePopup('#modulePopup')">
                <div class="panel">
                    <h2 class="@{{ module.class }}"><i class="fa fa-@{{ module.icon }}"></i> @{{ module.name }}</h2>
                    <p>@{{ module.description }}</p>
                    <button class="button black" ng-click="togglePopup('#modulePopup')">Close</button>
                </div>
            </div>
            <div class="popup" id="imgPopup" ng-click="togglePopup('#imgPopup')">
                <div class="panel align-center">
                    <img src="@{{ img }}">
                    <br>
                    <br>
                    <button class="button black" ng-click="togglePopup('#imgPopup')">Close</button>
                </div>
            </div>
        </section>
        <section id="portfolio" class="inverted">
            <h2>Web Sites and Apps Designed and Built By {{ config('site.company_name') }}</h2>
            <div class="thumbnails">
                <?php
                    $path = '/img/portfolio/';
                    $files = scandir(public_path() . $path . 'sm');
                    foreach($files as $file) {
                        if ($file !== '.' && $file !== '..') {
                ?>
                    <div class="thumbnail" ng-click="toggleImg('{{ $path . 'lg/' . $file }}')">
                        <div>
                            <img src="{{ $path . 'sm/' . $file }}">
                        </div>
                    </div>
                <?php } } ?>
            </div>
        </section>
        {{-- <section id="pricing">
            <h2>Packages &amp; Pricing</h2>
            <h3>Website Packages</h3>
            <p>Packages cover service hours by {{ config('site.company_name') }}. Products or services provided by third parties, such as domain name registration, hosting fees, and SSL certificates, are charged seprately. The following packages are for standard eCommerce websites. <a href="#additional-services">Click here for custom web apps</a>.</p>
            <div id="packages">
                <div class="row">
                    <div ng-repeat="package in packages" class="col-4">
                        <div ng-class="{ 'border': $index == 1 }" class="panel">
                            <div class="panel-header">
                                <h3>@{{ package.name }}</h3>
                            </div>
                            <ul>
                                <li class="fa fa-check" ng-repeat="service in services" ng-show="inArray($index, package.services)">@{{ service }}</li>
                            </ul>
                            <div class="panel-footer">
                                <p class="price">$@{{ package.price }}</p>
                                <a href="#contact" class="button" ng-click="selectPackage(package)">Select Package</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> --}}
        {{-- <section id="additional-services" class="gray">
            <div class="row">
                <div class="col-6">
                    <h3>Custom Web App Development</h3>
                    <p><strong>Custom development, maintenance, and updates</strong> are billed at only $50 / hr (roughly half the industry standard for app development). You'll be hard-pressed to find a better deal. You absolutely won't find better service.</p>
                    <a class="button" href="#contact">Get a Free Quote</a>
                </div>
                <div class="col-6">
                    <h3>Hosting by {{ config('site.company_name') }}</h3>
                    <p><strong>If you prefer to handle hosting yourself, no problem.</strong> We'll help you get set up with a service of your choice, and then you'll handle the hosting fees. If you'd like us to take the stress of your plate and handle hosting for you, even better. For only $50 / month, you'll get:</p>
                    <ul>
                        <li class="fa fa-check">Unlimited hosting</li>
                        <li class="fa fa-check">Updates and maintenance as needed</li>
                        <li class="fa fa-check">Up to 2 hours per month development time</li>
                    </ul>
                    <p>Additional hours, as requested by you, will be billed at our regular rate.</p>
                </div>
            </div>
        </section> --}}
    </div>
@stop
