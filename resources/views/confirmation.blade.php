@extends('layouts.default')
@section('content')
    <div class="alert success">
        Thank you for contacting us! We'll get back to you within one business day.
    </div>
@stop
@section('footer_scripts')
    <!-- Google Code for Complete Quote Request Form 2 Conversion Page -->
    <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 844211402;
    var google_conversion_language = "en";
    var google_conversion_format = "3";
    var google_conversion_color = "ffffff";
    var google_conversion_label = "T1XsCL3Lh3QQysnGkgM";
    var google_remarketing_only = false;
    /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/844211402/?label=T1XsCL3Lh3QQysnGkgM&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>
@stop
