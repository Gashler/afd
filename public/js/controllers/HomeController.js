var app = angular.module('app', [])
.controller('HomeController', function($scope, $http) {

    // define variables
    $scope.submitting = false;
    $scope.submitted = false;
    $scope.form = {};
    $scope.animating = false;

    $scope.services = [
        "Build Your Templated Wordpress Website",
        "Up to 5 pages, blog, contact form",
        "Deploy Your Website on a Server",
        "Set up account with payment processer",
        "Integrate WooCommerce system",
        "Integrate Google Analytics & Other Third Party Services",
        "Complete Custom Theme for Your Business",
        "10 additional hours of custom development"
    ];

    $scope.packages = [
        {
            name: 'Basic Web Site',
            services: [0,1,2],
            price: '599'
        },
        {
            name: 'eCommerce Website',
            services: [0,1,2,3,4,5],
            price: '999'
        },
        {
            name: 'Advanced Website',
            services: [0,1,2,3,4,5,6,7],
            price: '1499'
        }
    ];

    $scope.modules = [
        {
            name: 'CRM',
            icon: 'user',
            class: 'color-1',
            description: "Customer Relations Management - manage and communicate with prospects, leads, and customers."
        },
        {
            name: 'CMS',
            icon: 'files-o',
            class: 'color-2',
            description: "Content Management System - create and manage web site pages, posts, and media."
        },
        {
            name: 'Payments',
            icon: 'credit-card',
            class: 'color-3',
            description: "Take one-time payments or create ongoing subscriptions for members."
        },
        {
            name: 'Mobile',
            icon: 'mobile',
            class: 'color-4',
            description: "Build responsive/mobile friendly web apps or port your site to a native mobile app. Control and update both web and native apps through a single deployment."
        },
        {
            name: 'Party Planning',
            icon: 'calendar-check-o',
            class: 'color-1',
            description: "Create, manage, promote, and book events with attendees. Integrate your events with eCommerce and affiliate solutions."
        },
        {
            name: 'Reports',
            icon: 'line-chart',
            class: 'color-2',
            description: "Collect all kinds of data and generate the reports you need to run your business."
        },
        {
            name: 'Web Site',
            icon: 'globe',
            class: 'color-3',
            description: "Get a custom-built or templated web site with everything you need to promote your business, educate your prospects, and generate leads."
        },
        {
            name: 'Lead Generation',
            icon: 'check-square',
            class: 'color-4',
            description: "Create custom forms and offers for capturing leads and entering them into your marketing funnels."
        },
        {
            name: 'Marketing Automation',
            icon: 'envelope',
            class: 'color-1',
            description: "Nurture leads through email drip campaigns with segmented lists and automatic subscription management."
        },
        {
            name: 'Affiliates',
            icon: 'users',
            class: 'color-2',
            description: "Empower others to promote or resell your products. Create and manage single-level or multi-level distributor networks."
        },
        {
            name: 'Commissions',
            icon: 'dollar',
            class: 'color-3',
            description: "Reward your affiliates or representatives with automatic commissions and pay-outs built the way you want."
        },
        {
            name: 'eCommerce',
            icon: 'cubes',
            class: 'color-4',
            description: "Manage inventory and sell products with a custom eCommerce solution built to fit your needs."
        },
        {
            name: 'Office & Productivity',
            icon: 'sticky-note',
            class: 'color-1',
            description: "Get all the back office software you need to run internal operations for managers, employees, or affiliates."
        },
        {
            name: 'Multimedia',
            icon: 'play',
            class: 'color-2',
            description: "Give your users a rich, interactive media experience with endless possibilities for showcasing images, video, and audio."
        },
        {
            name: 'SMS Messaging',
            icon: 'mobile-phone',
            class: 'color-3',
            description: "Improve marketing results with easy tools for automatically sending out SMS messages to interested leads or customers."
        },
    ];

    $scope.selectPackage = function(package)
    {
        $scope.package = package;
        $scope.form.package_name = $scope.package.name;
        $scope.form.package_price = $scope.package.price;
        $scope.form.quote = $scope.package.price;
    }

    $scope.submitForm = function()
    {
        if (
            !$scope.form.Name
            || !$scope.form.Email
        ) {
            return alert("Please provide your name and email address.");
        }
        $scope.submitting = true;
        $http.post('/send-message', $scope.form).then(function(response) {
            window.location = '/message-sent';
        });
    }

    $scope.togglePopup = function(popup)
    {
        if (!$scope.animating) {
            $scope.animating = true;
            $(popup).fadeToggle(function() {
                $scope.animating = false;
            });
        }
    }

    $scope.toggleModule = function(module)
    {
        if (module) {
            $scope.module = module;
        }
        $('#modulePopup').fadeToggle();
    }

    $scope.toggleImg = function(img)
    {
        $scope.img = img;
        $('#imgPopup').fadeToggle();
    }

    $scope.inArray = function(needle, haystack)
    {
        var length = haystack.length;
        for(var i = 0; i < length; i++) {
            if(haystack[i] == needle) return true;
        }
        return false;
    }
});
