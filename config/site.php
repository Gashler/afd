<?php

return [
    'title' => 'AngelFox Development | We Build Next-generation Web Apps',
    'company_name' => 'AngelFox Development',
    'phone' => '(801) 900-3035',
    'email' => 'steve@stephengashler.com',
];
