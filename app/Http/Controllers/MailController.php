<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Order;
use App\Repositories\UserRepository;
use App\User;

class MailController extends Controller
{
    public function send()
    {
        $data = request()->all();
        \Mail::send('emails.new_client', [
            'data' => $data
        ], function ($m) {
            $m->from(config('mail.from_email'), config('site.company_name'));
            $m->to(config('mail.admin_email'), config('mail.admin_name'))->subject(config('site.company_name') . ' Quote Request');
        });
    }
}
