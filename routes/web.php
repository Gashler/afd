<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $local = request('local') ?? 0;
    return view('home', compact('local'));
});

Route::post('send-message', 'MailController@send');
Route::get('message-sent', function() {
    return view('confirmation');
});
Route::get('test', 'TestController@index');
